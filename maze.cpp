/************************************************************
Sokoban project - Maze file
Copyright Florent DIEDLER
Date : 27/02/2016

Please do not remove this header, if you use this file !
************************************************************/

#include "maze.h"
#include "graphic.h"
#include "utils/console.h"
#include "utils/coord.h"
#include <fstream>
#include <iomanip>
#include <queue>

Maze::Maze(const std::string& path)
    : m_lig(0), m_col(0), m_pos_player(0), m_dir_player(TOP), m_level_path(path)
{
}

Maze::~Maze()
{
}

bool Maze::init()
{
    bool res = this->_load(this->m_level_path);
    if (!res)
    {
        std::cerr << "Cannot load maze... Check file : " << this->m_level_path << std::endl;
        return false;
    }

    return res;
}

// Check if all boxes are on a goal
bool Maze::_isCompleted() const
{
    for (unsigned int i=0; i<this->m_pos_boxes.size(); ++i)
    {
        if (!this->isSquareBoxPlaced(this->m_pos_boxes[i]))
            return false;
    }
    return true;
}

// Check if we can push a box in a direction
// INPUT: position of the box to check, direction,
// OUTPUT : the position of the box after pushing
//      TRUE if all goes right otherwise FALSE
bool Maze::_canPushBox(unsigned short posBox, char dir, unsigned short& newPosBox) const
{
    // Check if this position is a box !
    if (!this->isSquareBox(posBox))
        return false;

    // Compute new position according to push direction
    newPosBox = Coord::getDirPos(posBox, dir);

    // Can we push the box ?
    return this->isSquareWalkable(newPosBox);
}

// Load a maze from a file (DO NOT TOUCH)
bool Maze::_load(const std::string& path)
{
    std::vector<unsigned short> tmpPosBoxes;
    std::ifstream ifs(path.c_str());
    if (ifs)
    {
        std::vector<std::string> lines;
        std::string line;
        while (std::getline(ifs, line))
        {
            lines.push_back(line);
            this->m_lig++;
            this->m_col = (this->m_col < line.size() ? line.size() : this->m_col);
        }
        ifs.close();

        if (this->m_col > NB_MAX_WIDTH || this->m_lig > NB_MAX_HEIGHT)
        {
            std::cerr << "Maze::load => Bad formatting in level data..." << std::endl;
            return false;
        }

        Coord::m_nb_col = this->m_col;
        for (unsigned int i=0; i<lines.size(); i++)
        {
            //LDebug << "Maze::load => Reading : " << lines[i];
            for (unsigned int j=0; j<this->m_col; j++)
            {
                if (j < lines[i].size())
                {
                    bool both = false;
                    unsigned short pos = Coord::coord1D(i, j);
                    unsigned char s = (unsigned char)(lines[i][j] - '0');

                    // Need to add a goal and a box ;)
                    if (s == SPRITE_BOX_PLACED)
                    {
                        both = true;
                    }

                    if (s == SPRITE_GOAL || both)
                    {
                        this->m_pos_goals.push_back(pos);
                    }
                    if (s == SPRITE_BOX || both)
                    {
                        tmpPosBoxes.push_back(pos);
                    }

                    // Assign player position
                    if (s == SPRITE_MARIO)
                    {
                        this->m_pos_player = pos;
                        //LDebug << "\tAdding player pos (" << pos << ")";
                        s = SPRITE_GROUND;
                    }

                    // Add this value in the field
                    this->m_field.push_back(s);
                }
                else
                {
                    // Here - Out of bound
                    this->m_field.push_back(SPRITE_GROUND);
                }
            }
        }

        // Copy box position
        this->m_pos_boxes.resize(tmpPosBoxes.size());
        for (unsigned int i=0; i<tmpPosBoxes.size(); ++i)
        {
            this->m_pos_boxes[i] = tmpPosBoxes[i];
        }
        DeadSquare();
        return (this->m_pos_boxes.size() == this->m_pos_goals.size());
    }
    else
    {
        std::cerr << "Maze::load => File does not exist..." << std::endl;
    }

    return false;
}

bool Maze::updatePlayer(char dir)
{   bool complet;
    if (dir < 0 || dir > MAX_DIR)
    {
        std::cerr << "Maze::updatePlayer => Direction not correct... " << +dir << std::endl;
        complet=false;
    }

    if(isSquareWalkable(Coord::getDirPos(getPosPlayer(),dir))|| isDeadSquare(Coord::getDirPos(getPosPlayer(),dir)))
    {
        setPlayerPos(Coord::getDirPos(getPosPlayer(), dir));
    }
    else if(isSquareBox(Coord::getDirPos(getPosPlayer(),dir)))
    {
        for(unsigned short i=0; i<m_pos_boxes.size(); i++)
        {
            if( m_pos_boxes[i]== (Coord::getDirPos(getPosPlayer(),dir)))
            {
                if(isSquareGround(Coord::getDirPos(getPosBoxes()[i],dir)))
                {
                    setSquare(Coord::getDirPos(getPosBoxes()[i],dir),2);
                    setSquare(getPosBoxes()[i],0);
                    setBoxePos(Coord::getDirPos(getPosBoxes()[i],dir),i);
                    setPlayerPos(Coord::getDirPos(getPosPlayer(), dir));
                }
                else if(isSquareGoal(Coord::getDirPos(getPosBoxes()[i],dir)))
                {
                    setSquare(Coord::getDirPos(getPosBoxes()[i],dir),3);
                    setSquare(getPosBoxes()[i],0);
                    setBoxePos(Coord::getDirPos(getPosBoxes()[i],dir),i);
                    setPlayerPos(Coord::getDirPos(getPosPlayer(),dir));
                }
                for(unsigned short a=0; a<m_pos_goals.size(); a++)
                {
                    if(isSquareBoxPlaced(m_pos_goals[a]))
                    {

                    }
                    else setSquare(m_pos_goals[a],4);
                }
            }
        }

    }
    this->m_dir_player=dir;
    complet=_isCompleted();
    return complet;
}

// Display maze on screen with Allegro
void Maze::draw(const Graphic& g) const
{
    for(unsigned int i=0; i<this->getSize(); i++)
    {
        unsigned int l = 0, c = 0;
        Coord::coord2D(i, l, c);

        if (i == this->m_pos_player)
        {
            g.drawT(g.getSpritePlayer(this->m_dir_player), c, l);
        }
        else
        {
            g.drawT(g.getSprite(this->m_field[i]), c, l);
        }
    }
}

// DO NOT TOUCH !
// Overload function for displaying debug information
// about Maze class
std::ostream& operator << (std::ostream& O, const Maze& m)
{
    unsigned int l, c;
    int i = 0;
    Coord::coord2D(m.m_pos_player, l, c);
    O << "Player position " << m.m_pos_player << " (" << l << "," << c << ")" << std::endl;
    O << "Field Size " << +m.m_lig << " x " << +m.m_col << " = " << m.getSize() << std::endl;
    O << "Field Vector capacity : " << m.m_field.capacity() << std::endl;
    O << "Field array : " << std::endl << std::endl;
    for(unsigned int l=0; l<m.getSize(); l++)
    {
        if (l == m.m_pos_player) Console::getInstance()->setColor(_COLOR_YELLOW);
        else if (m.isSquareWall(l)) Console::getInstance()->setColor(_COLOR_PURPLE);
        else if (m.isSquareBoxPlaced(l) || m.isSquareGoal(l)) Console::getInstance()->setColor(_COLOR_GREEN);
        else if (m.isSquareBox(l)) Console::getInstance()->setColor(_COLOR_BLUE);
        else if (m.m_field[l] == SPRITE_DEADSQUARE) Console::getInstance()->setColor(_COLOR_RED);
        else Console::getInstance()->setColor(_COLOR_WHITE);

        O << std::setw(2) << +m.m_field[l] << " "; // + => print as "int"

        if ((l+1) % m.m_col == 0)
        {
            O << std::endl;
        }
    }
    Console::getInstance()->setColor(_COLOR_DEFAULT);

    O << std::endl;
    O << "Box position : " << std::endl;
    for (unsigned int i=0; i<m.m_pos_boxes.size(); i++)
    {
        Coord::coord2D(m.m_pos_boxes[i], l, c);
        O << "\t" << "Box #" << i << " => " << std::setw(3) << m.m_pos_boxes[i] << std::setw(2) << " (" << l << "," << c << ")" << std::endl;
    }

    O << std::endl;
    O << "Goal position : " << std::endl;
    for (const auto& goal : m.m_pos_goals)
    {
        unsigned int l, c;
        Coord::coord2D(goal, l, c);
        if (m.isSquareBoxPlaced(goal)) Console::getInstance()->setColor(_COLOR_GREEN);
        O << "\t" << "Goal #" << i << " => " << std::setw(3) << goal << std::setw(2) << " (" << l << "," << c << ")" << std::endl;
        if (m.isSquareBoxPlaced(goal)) Console::getInstance()->setColor(_COLOR_DEFAULT);
        i++;
    }

    return O;
}

void Maze::DeadSquare()
{

    bool deadsquare1=false;
    bool murhaut=true;
    bool murbas=true;



    for(unsigned short i=Coord::m_nb_col; i<m_field.size(); i++) ///on parcourt toutes les cases de la matrice
    {
        ///les 4 "if" permette de regarder si une case est enour� par 2 mur
        /// si c'est vrai alors on les trnasforme en deadsquare
        if(isSquareGround(i) && isSquareWall(i-1) && isSquareWall(i-Coord::m_nb_col))
        {
            setSquare(i,9);
        }
        else if(isSquareGround(i) && isSquareWall(i-1) && isSquareWall(i+Coord::m_nb_col))
        {
            setSquare(i,9);
        }
        else if(isSquareGround(i) && isSquareWall(i+1) && isSquareWall(i-Coord::m_nb_col))
        {
            setSquare(i,9);
        }
        else if(isSquareGround(i) && isSquareWall(i+1) && isSquareWall(i+Coord::m_nb_col))
        {
            setSquare(i,9);
        }
    }

    for(unsigned short i=Coord::m_nb_col+1; i<(m_field.size()-Coord::m_nb_col); i++)
    {
        ///si une case est un deadlock
        if(isDeadSquare(i))
        {

            for(unsigned short j=i+1; j<(((i/Coord::m_nb_col)+1)*Coord::m_nb_col); j++)
            {
                ///on regarde a partir de quand on trouve un autre deadlock
                if(isDeadSquare(j))
                {

                    deadsquare1=true;
                }

                if(deadsquare1)
                {
                    /// et si il y a des murs au dessus et/ou en dessous
                    for(unsigned short a=i+1; a<j; a++)
                    {

                        if(!isSquareWall(a-Coord::m_nb_col))
                        {
                            murhaut=false;
                        }
                        if(!isSquareWall(a+Coord::m_nb_col))
                        {
                            murbas=false;
                        }
                    }
                    ///si les conditions sont remplies alors les cases entre les deux deadlocks
                    /// se transforment aussi en deadlocks
                    if(murhaut || murbas)
                    {
                        for(unsigned short b=i+1; b<j; b++)
                        {
                            if(isSquareGround(b))
                            {
                                setSquare(b,9);
                            }
                        }
                    }
                }
                deadsquare1=false;
            }
        }

        murhaut=true;
        murbas=true;
    }
    bool murdroite=true;
    bool murgauche=true;

    bool deadsquare2=false;

    for(unsigned short i=Coord::m_nb_col+1; i<m_field.size()-Coord::m_nb_col; i++)
    {
        if(isDeadSquare(i))
        {
            for(unsigned short j=i+Coord::m_nb_col; j<m_field.size(); j=j+Coord::m_nb_col)
            {
                if(isDeadSquare(j))
                {
                    deadsquare2=true;
                }
                if(deadsquare2)
                {
                    for(unsigned short a=i; a<j ; a=a+Coord::m_nb_col)
                    {
                        if(!isSquareWall(a-1) || isSquareBox(a))
                        {
                            murgauche=false;
                        }
                        if(!isSquareWall(a+1)|| isSquareBox(a))
                        {
                            murdroite=false;
                        }

                    }
                    if((murdroite|| murgauche))
                    {
                        for(unsigned short a=i; a<j; a=a+Coord::m_nb_col)
                        {
                            if(isSquareGround(a))
                            {
                                setSquare(a,9);
                            }
                        }
                    }
                }
                deadsquare2=false;
            }


        }
            murdroite=true;
            murgauche=true;
    }
}

std::vector<unsigned char> Maze:: bruteforce()
{
    unsigned short tempplayer=getPosPlayer();
    bool gagne=false;
    bool gagne2=false;
    unsigned short pos_deb=m_pos_player;
    std::vector<unsigned char> field_deb=m_field;
    std::vector<unsigned short> box_deb= m_pos_boxes;


    int compteur=1;
    int temp;
    unsigned short parcour=0;
    char direction=TOP;
    std::vector<unsigned short> chemin;
    std::vector<unsigned char> cfinal;
    std::vector<unsigned short> tempbox;
    chemin.resize(compteur,0);


    while (gagne==false)
    {


        m_pos_boxes=box_deb;            ///AVANT CHAQUE TEST JE REMET LE JEU DANS SON �TAT ORIGINEL
        m_field=field_deb;
        m_pos_player=pos_deb;
        for(unsigned short i=0; i<chemin.size(); i++)
        {
            if(chemin[i]==0)direction=TOP;
            if(chemin[i]==1)direction=BOTTOM;
            if(chemin[i]==2)direction=LEFT;
            if(chemin[i]==3)direction=RIGHT;
       //     std::cout<<chemin[i]<<"-";
            gagne=updatePlayer(direction);

        }




        parcour=0;
        if(!gagne)
        {
            while(chemin[parcour]==3&&parcour!=chemin.size())
            {
                parcour++;
            }
             if(parcour==chemin.size()){
                compteur++;
                chemin.resize(compteur);

                for(unsigned short i=0;i<chemin.size();i++){
                    chemin[i]=0;
                }
            }
            else
            {
                gagne2=false;
                chemin[0]++;
                if(chemin[0]==4)
                {
                    chemin[0]=0;
                    gagne2=true;
                }
                       if(gagne2)
                {
                    for(unsigned short i=1; i<chemin.size(); i++)
                    {
                        if(gagne2)
                        {
                            gagne2=false;
                            chemin[i]++;
                            if (chemin[i]==4)
                            {
                                gagne2++;
                                chemin[i]=0;
                            }
                        }
                    }
                }

              }
         }


    }
     for(unsigned short i=0;i<chemin.size();i++)
  {
      if(chemin[i]==0) cfinal.push_back(TOP);
      if(chemin[i]==1) cfinal.push_back(BOTTOM);
      if(chemin[i]==2) cfinal.push_back(LEFT);
      if(chemin[i]==3) cfinal.push_back(RIGHT);

  }

    std::cout<<chemin.size()<<std::endl;
    m_field=field_deb;
    m_pos_boxes=box_deb;
    m_pos_player=pos_deb;           ///AVANT D'ENVOYER LE CHEMIN,JE REMET LE JEU DANS SON �TAT ORIGINEL
  return cfinal;
}
